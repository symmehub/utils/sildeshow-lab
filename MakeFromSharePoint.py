
import os
import tempfile
from office365.runtime.auth.authentication_context import AuthenticationContext
from office365.sharepoint.client_context import ClientContext
from office365.sharepoint.files.file import File
from office365.sharepoint.folders.folder import Folder

# récupération des mots de passe
from mdp_ER import sharepoint_user, sharepoint_password

#Constrtucting SharePoint URL
sharepoint_base_url = 'https://univsmb.sharepoint.com/sites/SYMME/'
folder_in_sharepoint = '/sites/SYMME/Shared%20Documents/General/Com/AffichageEcrans'

#Constructing Details For Authenticating SharePoint
auth = AuthenticationContext(sharepoint_base_url)

auth.acquire_token_for_user(sharepoint_user, sharepoint_password)
ctx = ClientContext(sharepoint_base_url, auth)
web = ctx.web
ctx.load(web)
ctx.execute_query()
print('Connected to SharePoint: ',web.properties['Title'])

   
#Constructing Function for getting file details in SharePoint Folder

def folder_details(ctx, folder_in_sharepoint):
  folder = ctx.web.get_folder_by_server_relative_url(folder_in_sharepoint)
  fold_names = []
  sub_folders = folder.files 
  ctx.load(sub_folders)
  ctx.execute_query()
  for s_folder in sub_folders:
    fold_names.append(s_folder.properties["ServerRelativeUrl"])
  return fold_names

def supprimer_fichiers_dossier(dossier):
    if os.path.exists(dossier) and os.path.isdir(dossier):
        fichiers = os.listdir(dossier)

        for fichier in fichiers:
            chemin_complet = os.path.join(dossier, fichier)
            if os.path.isfile(chemin_complet):
                os.remove(chemin_complet)
            else:
                print(f"Le fichier {fichier} n'a pas été supprimé car il n'est pas un fichier.")

        print("Tous les fichiers ont été supprimés avec succès.")
    else:
        print("Le dossier spécifié n'existe pas.")

 
#Getting folder details

file_list = folder_details(ctx, folder_in_sharepoint)
download_dir = './src/images_imported'
supprimer_fichiers_dossier(download_dir)
for file_url in file_list:
    download_path = os.path.join(download_dir, os.path.basename(file_url))
    with open(download_path, "wb") as local_file:
        file = (
            ctx.web.get_file_by_server_relative_url(file_url)
            .download(local_file)
            .execute_query()
        )
    print("[Ok] file has been downloaded into: {0}".format(download_path))

## MAKE SLIDE SHOW
# Dossier contenant les images
image_directory = './src/images_imported'
relative_image_directory = 'images_imported'

# Générer la liste des noms de fichiers d'images dans le dossier
image_files = [f for f in os.listdir(image_directory) if f.endswith(('.jpg', '.jpeg', '.png', '.gif'))]

# Générer le contenu HTML du diaporama
def generate_slideshow(images):
    slideshow_html = '''
<!DOCTYPE html>
<html>
<head>
    <title>Slideshow d'Images</title>
    <style>
        .slideshow-container {
            max-width: 1200px;
            position: relative;
            margin: auto;
        }

        .mySlides {
            display: none;
        }

        img {
            width: 100%;
        }
    </style>
</head>
<body>

<div class="slideshow-container">
'''

    for image in images:
        image_path = os.path.join(relative_image_directory, image)
        slideshow_html += f'''
    <div class="mySlides">
        <img src="{image_path}" alt="Image">
    </div>
'''

    slideshow_html += '''
</div>

<script>
    let slideIndex = 0;
    showSlides();

    function showSlides() {
        let i;
        const slides = document.getElementsByClassName("mySlides");
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        slideIndex++;
        if (slideIndex > slides.length) {
            slideIndex = 1;
        }
        slides[slideIndex - 1].style.display = "block";
        setTimeout(showSlides, 6000); // Change l'image toutes les n ms
    }
</script>

</body>
</html>
'''

    return slideshow_html

# Chemin de sortie pour le fichier HTML
output_file = "src/index.html"

# Générer le contenu HTML du diaporama
slideshow_content = generate_slideshow(image_files)

# Écrire le contenu HTML dans un fichier de sortie
with open(output_file, 'w') as output:
    output.write(slideshow_content)

print(f'Le diaporama a été généré dans {output_file}')
